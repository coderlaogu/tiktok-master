package com.tiktok.controller;

import com.tiktok.result.GraceJSONResult;
import com.tiktok.utils.SMSUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 *
 * @author 老顾
 * @since 2023-05-26
 */
@RestController
@Api(tags = "hello 测试的路由")
public class HelloController {

    @Resource
    private SMSUtils smsUtils;

    @ApiOperation(value = "这是一个hello的测试路由")
    @GetMapping("/hello")
    public Object hello() {
        return GraceJSONResult.ok("hello springboot");
    }

    @GetMapping("/sms")
    public Object sms() {
        String code = "123456";
        try {
            smsUtils.sendSMS("15328965713",code);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return GraceJSONResult.ok();
    }
}

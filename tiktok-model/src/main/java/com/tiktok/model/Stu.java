package com.tiktok.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author 老顾
 * @since 2023-05-25
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Stu {

    private String name;
    private Integer age;
}

package com.tiktok.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * <p>
 * 更新用户信息
 * </p>
 *
 * @author 老顾
 * @since 2023-05-25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UpdatedUserBO {

    private String id;
    private String nickname;
    private String imoocNum;
    private String face;
    private Integer sex;
    private Date birthday;
    private String country;
    private String province;
    private String city;
    private String district;
    private String description;
    private String bgImg;
    private Integer canImoocNumBeUpdated;
}

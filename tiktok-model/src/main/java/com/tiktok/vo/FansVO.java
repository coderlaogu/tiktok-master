package com.tiktok.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author 老顾
 * @since 2023-05-25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FansVO {

    private String fanId;
    private String nickname;
    private String face;
    private boolean isFriend = false;
}

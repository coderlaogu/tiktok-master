package com.tiktok.exceptions;
import com.tiktok.result.ResponseStatusEnum;

/**
 * 优雅地处理异常，统一封装
 */
public class GraceException {

    public static void display(ResponseStatusEnum responseStatusEnum) {
        throw new MyCustomException(responseStatusEnum);
    }

}

package com.tiktok.my.mapper;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * <p>
 * 继承自己的MyMapper
 * </p>
 *
 * @author 老顾
 * @since 2023-05-25
 */
public interface MyMapper<T> extends Mapper<T>, MySqlMapper<T> {
}

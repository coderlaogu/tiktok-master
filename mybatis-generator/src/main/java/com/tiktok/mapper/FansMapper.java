package com.tiktok.mapper;

import com.tiktok.my.mapper.MyMapper;
import com.tiktok.pojo.Fans;

public interface FansMapper extends MyMapper<Fans> {
}
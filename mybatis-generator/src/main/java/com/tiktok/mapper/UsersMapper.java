package com.tiktok.mapper;

import com.tiktok.my.mapper.MyMapper;
import com.tiktok.pojo.Users;

public interface UsersMapper extends MyMapper<Users> {
}
package com.tiktok.mapper;

import com.tiktok.my.mapper.MyMapper;
import com.tiktok.pojo.Vlog;

public interface VlogMapper extends MyMapper<Vlog> {
}
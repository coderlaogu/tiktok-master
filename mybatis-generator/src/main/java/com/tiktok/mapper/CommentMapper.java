package com.tiktok.mapper;

import com.tiktok.my.mapper.MyMapper;
import com.tiktok.pojo.Comment;

public interface CommentMapper extends MyMapper<Comment> {
}
package com.tiktok.service;

import com.tiktok.bo.UpdatedUserBO;
import com.tiktok.pojo.Users;

/**
 * <p>
 * 用户业务层
 * </p>
 *
 * @author 老顾
 * @since 2023-05-25
 */
public interface UserService {

    /**
     * 判断用户是否存在，如果存在则返回用户信息
     *
     * @param mobile 手机号
     * @return 用户信息
     */
    Users queryMobileIsExist(String mobile);


    /**
     * 创建用户信息，并且返回用户对象
     *
     * @param mobile 手机号
     * @return 用户对象
     */
    Users createUser(String mobile);

    /**
     * 根据用户主键查询用户信息
     *
     * @param userId 用户id
     * @return 用户信息
     */
    Users getUser(String userId);

    /**
     * 修改用户信息
     *
     * @param updatedUserBO 用户信息请求参数
     * @param type 用户类型
     * @return 用户信息
     */
    Users updateUserInfo(UpdatedUserBO updatedUserBO, Integer type);

    Users updateUserInfo(UpdatedUserBO updatedUserBO);
}

package com.tiktok.service;

import com.tiktok.bo.CommentBO;
import com.tiktok.pojo.Comment;
import com.tiktok.vo.CommentVO;

/**
 * <p>
 *
 * </p>
 *
 * @author 老顾
 * @since 2023-05-25
 */
public interface CommentService {
    CommentVO createComment(CommentBO commentBO);

    Object queryVlogComments(String vlogId, String userId, Integer page, Integer pageSize);

    void deleteComment(String commentUserId, String commentId, String vlogId);

    Comment getcomment(String commentId);
}

package com.tiktok.service;

import com.tiktok.bo.VlogBO;
import com.tiktok.pojo.Vlog;
import com.tiktok.utils.PagedGridResult;

/**
 * <p>
 *
 * </p>
 *
 * @author 老顾
 * @since 2023-05-25
 */
public interface VlogService {

    /**
     * 新增vlog视频
     */
    void createVlog(VlogBO vlogBO);

    /**
     * 查询首页/搜索的vlog列表
     */
    PagedGridResult getIndexVlogList(String userId, String search, Integer page, Integer pageSize);

    /**
     * 获得用户点赞视频的总数
     */
    public Integer getVlogBeLikedCounts(String vlogId);

    /**
     * 根据视频主键查询vlog
     */
    Object getVlogDetailById(String userId, String vlogId);
    /**
     * 用户把视频改为公开/私密的视频
     */
    void chanToPrivateOrPublic(String userId, String vlogId, Integer yesOrNo);

    /**
     * 查询用的公开/私密的视频列表
     */
    PagedGridResult queryMyVlogList(String userId, Integer page, Integer pageSize, Integer type);

    /**
     * 查询用户点赞过的短视频
     */
    PagedGridResult getMyLikedVlogList(String userId, Integer page, Integer pageSize);

    /**
     * 用户点赞/喜欢视频
     */
    void userLikeVlog(String userId, String vlogId);

    /**
     * 根据主键查询vlog
     */
    Vlog getVlog(String id);

    /**
     * 用户取消点赞/喜欢视频
     */
    void userUnLikeVlog(String userId, String vlogId);

    /**
     * 查询用户关注的博主发布的短视频列表
     */
    PagedGridResult getMyFollowVlogList(String myId, Integer page, Integer pageSize);

    /**
     * 查询朋友发布的短视频列表
     */
    PagedGridResult getMyFriendVlogList(String myId, Integer page, Integer pageSize);
}

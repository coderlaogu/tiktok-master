package com.tiktok.service;

import com.tiktok.mo.MessageMO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author 老顾
 * @since 2023-05-25
 */
public interface MsgService {

    /**
     * 创建消息
     */
    void createMsg(String userId, String vlogerId, Integer type, Map msgContent);

    /**
     * 查询消息列表
     */
    List<MessageMO> queryList(String userId, Integer page, Integer pageSize);
}

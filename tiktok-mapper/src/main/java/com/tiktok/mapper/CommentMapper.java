package com.tiktok.mapper;

import com.tiktok.my.mapper.MyMapper;
import com.tiktok.pojo.Comment;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentMapper extends MyMapper<Comment> {
}
package com.tiktok.mapper;

import com.tiktok.my.mapper.MyMapper;
import com.tiktok.pojo.MyLikedVlog;
import org.springframework.stereotype.Repository;

@Repository
public interface MyLikedVlogMapper extends MyMapper<MyLikedVlog> {
}
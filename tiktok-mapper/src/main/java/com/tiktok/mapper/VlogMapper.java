package com.tiktok.mapper;

import com.tiktok.my.mapper.MyMapper;
import com.tiktok.pojo.Vlog;
import org.springframework.stereotype.Repository;

@Repository
public interface VlogMapper extends MyMapper<Vlog> {
}